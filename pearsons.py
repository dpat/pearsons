#!/usr/bin/env python3
#
# Implement Pearson's correlation coefficient.
#   Written by: Dianne Patterson. 121/14/2019.
#   Last Modified: Initial creation.
#
import os
import sys
import math

def usage(argv):
  print("Usage: {0} time-series1 time-series2".format(argv[0]), file=sys.stderr)
  print("where: each time series file contains a column of numbers", file=sys.stderr)


# read a column of numbers from the given file and return a vector of floats
def read_vector(filename):
  with open(filename) as fyl:               # closes file when done
    vec_txt = fyl.readlines()               # read all the lines at once into a text vector
    return [ float(val.strip()) for val in vec_txt ] # list comprehension


def main(argv):
  if (len(argv) < 3):                       # check for the two required filenames
    usage(argv)
    exit(1)

  filename_x = argv[1]
  filename_y = argv[2]

  x = read_vector(filename_x)               # convert text vector to float vector
  y = read_vector(filename_y)               # convert text vector to float vector

  n_x = len(x)
  n_y = len(y)
  if (n_x != n_y):
    print("ERROR: Each time series file must be the same length:", file=sys.stderr)
    print("       File {0} has length {1} and file {2} has length {3}".format(
      filename_x, n_x, filename_y, n_y), file=sys.stderr)
    exit(2)
  n = n_x                                   # just for algorithm clarity

  sum_x = sum(x)
  sum_y = sum(y)
  xy = [ a*b for a, b in zip(x, y) ]
  sum_xy = sum(xy)
  xsq = [ a*b for a, b in zip(x, x) ]
  ysq = [ a*b for a, b in zip(y, y) ]
  sum_xsq = sum(xsq)
  sum_ysq = sum(ysq)

  r = ( (n * sum_xy - sum_x * sum_y) /
        math.sqrt(((n * sum_xsq) - (sum_x * sum_x)) * ((n * sum_ysq) - (sum_y * sum_y))) )

  print("{0}, {1}, {2}".format(filename_x, filename_y, r))



# this makes it a self-running script
if __name__ == "__main__":
    main(sys.argv[:])                       # pass all CLI arguments
