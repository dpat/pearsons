#!/bin/bash

LIST=${1}.txt
stem=`basename -s .txt ${LIST}`
outimg=${stem}.nii.gz

for img in `cat ${LIST}`; do
# cheap trick to make an empty mask the right size
   	if [ ! -e  ${outimg} ]; then
      imcp HO-CB/${img} ${outimg}
      echo "creating mask from ${img}"
# subtract the image from itself to get an empty image.
      fslmaths ${outimg} -sub ${outimg} ${outimg}
   	fi
     echo "${img}"
   	# Add the next image in the set to the outimg file, name it outimg again, repeat.
     fslmaths ${outimg} -add HO-CB/${img} ${outimg}
     fslmaths ${outimg} -bin ${outimg}
done
